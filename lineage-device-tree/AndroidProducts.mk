#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_yoga11.mk

COMMON_LUNCH_CHOICES := \
    lineage_yoga11-user \
    lineage_yoga11-userdebug \
    lineage_yoga11-eng
