#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from yoga11 device
$(call inherit-product, device/lenovo/yoga11/device.mk)

PRODUCT_DEVICE := yoga11
PRODUCT_NAME := omni_yoga11
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := Lenovo YT-J706F
PRODUCT_MANUFACTURER := lenovo

PRODUCT_GMS_CLIENTID_BASE := android-lenovo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_yoga11-user 12 SP1A.210812.016 YT-J706F_S240133_230206_ROW release-keys"

BUILD_FINGERPRINT := Lenovo/YT-J706F/YT-J706F:12/SP1A.210812.016/YT-J706F_S240133_230206_ROW:user/release-keys
